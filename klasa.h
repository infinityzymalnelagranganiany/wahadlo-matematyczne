///// PLIK Z KLASA WAHADLO I KLASA PAROMETRY KTORA PRZYDALA SIE W FUNKCJI Z ROWNANIEM
#include <iostream>
#include <iomanip>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <windows.h>
#include <vector>
#include <fstream>
#include <conio.h>

using namespace std;

class Wahadlo
{
/////ZMIENNE/////
    double fi0;             //KAT POCZATKOWY
    double dfidt0;          //PREDKOSC POCZATKOWA
    double t0;              //CZAS POCZATKOWY
    double tkoncowe;        //CZAS KONCOWY
    double interwal;        //"KRORK"
    double l;               //DLUGOSC WAHADLA
    double gamma;           //WSPOLCZYNNIK WYMUSZENIA
    double beta;            //WSPOLCZYNNIK TLUMIENIA
    double masa;            // XD
    double g;               //PRZYSPIESZNIE GRAWITACYJNE
    double rawData[2];      // KAT I PREDKOSC

/////KONSTRUKTORY (DOMYSLNY I ZWYKLY)/////
    public:
    Wahadlo() : fi0(0), dfidt0(0), t0(0), tkoncowe(0), interwal(0), l(0), gamma(0), beta(0), masa(0) ,g(0) {};
    Wahadlo(double _fi0, double _dfidt0, double _t0, double _tkoncowe, double _interwal, double _l, double _gamma, double _beta, double _masa, double _g)
    {
        fi0=_fi0;
        dfidt0=_dfidt0;
        t0=_t0;
        tkoncowe=_tkoncowe;
        interwal=_interwal;
        l=_l;
        masa=_masa;
        gamma=_gamma;
        beta=_beta;
        g=_g;
    }

/////METODY/////
        void wczytajDane();
        void uruchom();
        void zapiszDoPliku();
        void autorzy();

/////VEKTORY NA WYNIKI/////
        vector<double> wynikifi;
        vector<double> wynikiomega;
        vector<double> trwanie;
};





class P
{
   public:
        double      t0;
        double      interwal;
        double      l;
        double      masa;
        double      tkoncowe;
        double      gamma;
        double      beta;
        double      g;

/////KONSTRUKTOR/////
  P ( double _t0, double _tkoncowe, double _interwal, double _l, double _masa, double _gamma, double _beta, double _g)
{
        t0=_t0;
        interwal=_interwal;
        l=_l;
        masa=_masa;
        tkoncowe=_tkoncowe;
        gamma=_gamma;
        beta=_beta;
        g=_g;
}

};
