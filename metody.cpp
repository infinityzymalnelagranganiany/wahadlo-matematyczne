#include <iostream>
#include <math.h>
#include "klasa.h"

using namespace std;


/////METODA PYTAJACA UZYTKOWNIKA O DANE (ZAJME SIE TYM, BEDZIE LEPIEJ XD)/////
/*void Wahadlo::wczytajDane()
{
    cout << "just THE GREATEST, FASTEST, MOST CHARMING PENDULUM SIMULATOR EVER!!!!\n\nNasz symulator pozwala wybrac nastepujace dane : " << endl;

    cout << "1. Czas poczatkowy" << endl;
    cout << "2. Czas koncowy" << endl;
    cout << "3. Predkosc poczatkowa" << endl;
    cout << "4. Kat poczatkowy" << endl;
    cout << "5. Dlugosc wahadla" << endl;
    cout << "6. Masa wahadla" << endl;
    cout << "7. Wspolczynnik tlumienia" << endl;
    cout << "8. Interwal (krok)" << endl;
    cout << "Press any key to start";
    getch();
    system("cls");

    cout << "Podaj mase wahadla : ";                        cin >> m;                           system("cls");
    cout << "Podaj dlugosc ramienia wahadla : ";            cin >> l;                           system("cls");
    cout << "Podaj wyhylenie poczatkowe (w radianch) : ";  cin >> fi0;                          system("cls");
    cout << "Podaj predkosc poczatkowa : ";                 cin >> dfidt0;                      system("cls");
    cout << "Podaj wspolczynnik tlumienia : ";              cin >> k;                           system("cls");
    cout << "Podaj czas poczatkowy : ";                     cin >> t0;                          system("cls");
    cout << "Podaj dlugosc dzialania programu : ";          cin >> tkoncowe;    tkoncowe+=t0;   system("cls");
    cout << "Podaj interwal obliczen (w sekundach) : ";     cin >> interwal;                    system("cls");
}*/



/////FUNKCJA UZYWANA W ODE TU ZAPISUJE SIE WZOR BEDZIE UZYTA W METODZIE URUCHOM/////
int funkcja (double t, const double fi[], double d2fidt2[], void* parametry)
{
    P p = * (P*) parametry;

    d2fidt2[0] = fi[1];
////// WZOR  /////
///// JEDEN  /////
    d2fidt2[1] = ( p.gamma*sin(fi[1]*t)/p.masa ) - ( (p.g/p.l)*fi[0]) - (2*p.beta*fi[1]) ;

    return GSL_SUCCESS;
}






/////METODA Z OBLICZENIAMI/////
void Wahadlo::uruchom()
{
////ZMIENNE (KAT I PREDKOSC)///// NA START WARTOSCI POCZATKOWE
    rawData[0] = fi0;
    rawData[1] = dfidt0;

///// OBIEKT DANYCH /////
    P ppp(t0, tkoncowe,interwal, l, masa , gamma, beta, g);


///// O D E /////
    gsl_odeiv2_system sys = {funkcja, NULL, 2, &ppp }; //POTRZEBUJA ONE TEJ FUNKCJI

///// ALOKACJA PAMIECI NA OBLICZZENIA /////
    gsl_odeiv2_driver *czarnamagia = gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rkf45, 1e-4, 1e-4, 0.0);


/////PETLA OBLICZEN (CO "KROK")/////
    for (int t=t0; t<tkoncowe; t+=interwal)
    {
        int czyWszystkoPoszloDobrze = gsl_odeiv2_driver_apply(czarnamagia, &t0, t, rawData); // WRZUCA WYNIKI DO rawData

/////WALIDACJA/////
        if (czyWszystkoPoszloDobrze!=GSL_SUCCESS)
        {
            cerr << "!!! BLAD !!!" << endl << czyWszystkoPoszloDobrze << endl;
            break;
        }

/////ZAPIS DO WEKTOROW/////
        wynikifi.push_back(rawData[0]);
        wynikiomega.push_back(rawData[1]);
        trwanie.push_back(t);

/////WYSWIETLENIE WYNIKOW/////
        cout << t << ";\t\t" << rawData[0] << ";\t\t" << rawData[1] << endl;
    }
    cout << "Press any key to save results";
    getch();
    system("cls");
}







/////ZAPIS Z WEKTORA DO PLIKU//// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
void Wahadlo::zapiszDoPliku()
{
    ofstream out;
    out.open("wyniki.csv");

    //out << "t, fi, omega" << endl;

    for(int i=0; i<wynikifi.size(); i++)
    {
        out << trwanie[i] <<",";
        out << wynikifi[i] << ",";
        out << wynikiomega[i] << ",";
        out << endl;
    }

    cout << "Udalo sie\nWyniki zapisano do pliku : \"wyniki.csv\"";
    out.close();
}

void Wahadlo::autorzy()
{
    Sleep(1000);
    cout << "\n\n\n\tAUTHORS:";
    Sleep(500);
    cout << "\n\nWojciech Kosnik-Kowalczuk";
    Sleep(500);
    cout << "\nEmilia Wlodek";
    Sleep(500);
    cout << "\nKuba Grabowski";
    Sleep(500);
    cout << "\nSzymon Leszczynski";
    Sleep(4321);
}















